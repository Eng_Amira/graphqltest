module Types
    class UserType < Types::BaseObject
      graphql_name "User"
  
      implements GraphQL::Types::Relay::Node
      #global_id_field :id
  
      field :username, String, null: false
      delegate :username, to: :object
    
      field :email, String, null: false

      field :password, String, null: false
      field :posts, [Types::PostType], null: true
      field :comments, [Types::CommentType], null: true

      
  
      field :authentication_token, String, null: false
      def authentication_token
        if object.gql_id != context[:current_user]&.gql_id
          raise GraphQL::UnauthorizedFieldError,
                "Unable to access authentication_token"
        end
  
        object.authentication_token
      end
    end
end