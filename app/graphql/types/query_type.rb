module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    # TODO: remove me
    # field :test_field, String, null: false,
    #   description: "An example field added by the generator"
    # def test_field
    #   "Hello World!"
    # end
    #field :all_posts, [PostType], null: false
    field :all_users, [UserType], null: false
    field :all_posts, resolver: Resolvers::PostsSearch
    field :user, Types::UserType, null: false do
      argument :id, ID, required: true
    end

    # def all_posts
    #   Post.all
    # end

    def all_users
      User.all
    end
    def user(id:)
      User.find(id)
    end
 

  end
end
