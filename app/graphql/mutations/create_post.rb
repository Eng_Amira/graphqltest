module Mutations
    class CreatePost < BaseMutation
      # arguments passed to the `resolve` method
      argument :title, String, required: true
      argument :content, String, required: true
  
      # return type from the mutation
      type Types::PostType
  
      def resolve(title: nil, content: nil)
        authorize_user
        Post.create!(
          title: title,
          content: content,
          user: context[:current_user]
          
        )
        #rescue ActiveRecord::RecordInvalid => e
            #GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
      end
    end
end