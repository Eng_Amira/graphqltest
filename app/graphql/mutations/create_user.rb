module Mutations
    class CreateUser < BaseMutation
      # arguments passed to the `resolve` method
      argument :username, String, required: true
      argument :email, String, required: true
      argument :password, String, required: true

  
      # return type from the mutation
      type Types::UserType
  
      def resolve(username: nil, password: nil, email: nil)
        User.create!(
            username: username,
            email: email,
            password: password,
          
        )
        #rescue ActiveRecord::RecordInvalid => e
            #GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
      end
    end
end