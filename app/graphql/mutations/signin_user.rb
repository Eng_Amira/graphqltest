module Mutations
    class SigninUser < BaseMutation
      null true
      # arguments passed to the `resolve` method
      argument :email, String, required: true
      argument :password, String, required: true

      field :user, Types::UserType, null: false

  
      def resolve(email: nil, password: nil)
        user = User.find_for_database_authentication(email: email)
  
        if user.present?
          if user.valid_password?(password)
            context[:session][:token] = user.authentication_token
            context[:current_user] = user
            MutationResult.call(obj: { user: context[:current_user] }, success: true)
          else
            GraphQL::ExecutionError.new("Incorrect Email/Password")
          end
        else
          GraphQL::ExecutionError.new("User not registered on this application")
        end
      end
    end
end