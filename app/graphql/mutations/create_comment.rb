module Mutations
    class CreateComment < BaseMutation
      # arguments passed to the `resolve` method
      argument :post_id, ID, required: false
      argument :user_id, ID, required: false
      argument :title, String, required: true
      argument :content, String, required: true
  
      # return type from the mutation
      type Types::CommentType
  
      def resolve(post_id: nil,title: nil, content: nil)
        Comment.create!(
          post: Post.find(post_id),
          user: User.find(user_id),
          title: title,
          content: content,
          user: context[:current_user]
          
        )
        #rescue ActiveRecord::RecordInvalid => e
            #GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
      end
    end
end