module Mutations
    class SignoutUser < Mutations::BaseMutation
 
      field :user, Types::UserType, null: false
      field :success, String, null: true
      field :errors, String, null: true
  
      def resolve
        user = context[:current_user]
        if user.present?
          success = user.reset_authentication_token!
  
          MutationResult.call(
            obj: { user: user },
            success: success,
            errors: user.errors
          )
        else
          GraphQL::ExecutionError.new("User not signed in")
        end
      end
    end
end